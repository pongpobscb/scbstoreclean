//
//  NetworkFetchTests.swift
//  SCBStoreCleanSwiftTests
//
//  Created by PONGPOB NGAMKANOKWAN on 14/5/2562 BE.
//  Copyright © 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import XCTest

@testable import SCBStoreCleanSwift

class NetworkFetchTests: XCTestCase {
    
    let timeout = 5
    var error : SCError?
    var mainVC : SCMainViewController!
    var mainWindow : UIWindow!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        mainWindow = UIWindow()
        setupMainVC()
        
    }
    
    func setupMainVC (){
        let bundle = Bundle.main
        let sb = UIStoryboard(name: "Main", bundle: bundle)
        mainVC = sb.instantiateViewController(withIdentifier: "SCMainViewController") as? SCMainViewController
    }
    

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testShouldFetchMainVCViewAppear() {
    
        
        let expectation = self.expectation(description: "All Phones Fetched")
        
        SCBTestNetworkAPI.fetchAllMobiles(completion: { (phones) in
            print("YO phones count is = \(phones.count)")
            expectation.fulfill()
        }) { (err) in
            self.error = err
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: TimeInterval(self.timeout))
        XCTAssertNil(error, "Error should be nil")
        // Then
        
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
