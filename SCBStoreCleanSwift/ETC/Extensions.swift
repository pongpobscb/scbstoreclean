//
//  Extension.swift
//  SCBStore
//
//  Created by XXD on 25/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func easypresent (toVC: String, completion : @escaping pushVCCompletion) {

        let sb = UIStoryboard(name: "Main", bundle: nil)
        let nextvc = sb.instantiateViewController(withIdentifier: toVC)
        self.present( nextvc, animated: true, completion: {
        completion(nextvc)
        })

    }

    func easypresentNav (toVC: String, completion : @escaping pushVCCompletion) {

        let sb = UIStoryboard(name: "Main", bundle: nil)
        let nextvc = sb.instantiateViewController(withIdentifier: toVC)

        let nav =  UINavigationController(rootViewController: nextvc)

        self.present( nav, animated: true, completion: {
        completion(nextvc)

        })
     }

    func easyPushSegue (toVC: String, completion : @escaping pushVCCompletion) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let nextvc = sb.instantiateViewController(withIdentifier: toVC)
        self.navigationController?.pushViewController(nextvc, animated: true)
        completion(nextvc)
    }

    func easyDismissSegue () {
        self.navigationController?.popViewController(animated: true)
    }

    func addCustomBackButton () {
        let backbutton = UIImage(named: "backButton")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backbutton,
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(backAction))
    }

    @objc private  func backAction() {
        self.easyDismissSegue()

    }

    func displayErrorAlert  (msg: String) {

        let options = UIAlertController(title: "Error",
                                        message: msg, preferredStyle: .alert)

        options.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in

        }))

        self.present(options, animated: true, completion: nil)
    }
}

extension UIImageView {

    func imageQuery ( address: String? ) -> Bool {

        guard let addressvalid = address else { return false}
        var httpsaddress: String = addressvalid

        if addressvalid.hasPrefix("https://") || addressvalid.hasPrefix("http://") {
           httpsaddress = addressvalid
        } else {
           httpsaddress = "https://" + addressvalid
        }
        guard let url = URL(string: httpsaddress) else { return false}
        let data = try? Data(contentsOf: url)
        if let mydata = data {
            self.image =  UIImage(data: mydata)
            return true
        }
        return false
    }
}

extension Notification.Name {

    static let FavoriteSelected = Notification.Name("FavoriteSelected")
    static let FavoriteDeselected = Notification.Name("FavoriteDeselected")
    static let FavoriteDeleted = Notification.Name("FavoriteDeleted")
}
