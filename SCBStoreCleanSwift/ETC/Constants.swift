//
//  Constants.swift
//  SCBStore
//
//  Created by XXD on 25/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import UIKit

//  Transition completion
typealias basicCompletion = () -> Void
typealias pushVCCompletion = (_ nextVC: UIViewController) -> Void

//  Network completion
public typealias FailureErrorBlock = (_ error: SCError) -> Void
public typealias ObjectCompletionBlock<T> = ( _ object: T ) -> Void

//  Parser completion
typealias MultiPhonesCompletion = (_ phones: [PhoneModel]) -> Void
typealias PhoneCompletion =  (PhoneModel) -> Void
typealias PhoneImageCompletion = ([PhoneImageModel]) -> Void

// ViewModel Completion

typealias MainViewModelCompletion = (MainViewModel) -> Void
typealias DetailViewModelCompletion = (DetailViewModel) -> Void
typealias DetailResponseCompletion = (DetailViewModel) -> Void

let SCBBaseURL = "https://scb-test-mobile.herokuapp.com"
let SCBAllMobilesPath = "/api/mobiles/"
let SCBSpecifyMobileImagesPath  = "/images/"

let  SCBAllmobileURLSample = "https://scb-test-mobile.herokuapp.com/api/mobiles/"
let  SCBImageslistURLSample = "https://scb-test-mobile.herokuapp.com/api/mobiles/1/images/"

enum displayMainView {
    case first
    case second
}

enum Sortby {
    case priceLowToHigh
    case priceHighToLow
    case ratingHighToLow
}
