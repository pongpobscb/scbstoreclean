//
//  PhoneImageModel.swift
//  SCBStore
//
//  Created by XXD on 27/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

struct PhoneImageModel {

    var imageURL: String? = ""
    var identity: Int? = 0

    init(imageURL: String, identity: Int) {

        self.imageURL = imageURL
        self.identity = identity

    }
}
