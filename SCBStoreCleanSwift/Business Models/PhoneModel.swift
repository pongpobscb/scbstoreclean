//
//  PhoneModel.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

struct PhoneModel {

    var name: String = ""
    var price: Float = 0.0
    var thumbnailURL: String? = ""
    var identity: Int = 0

    var brand: String = ""
    var rating: Float = 0.0
    var description: String = ""

    var isFavorite: Bool = false
}
