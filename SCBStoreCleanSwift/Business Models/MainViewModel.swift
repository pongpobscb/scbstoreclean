//
//  MainViewModel.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

class MainViewModel: NSObject , SCBaseViewModel {

    var model: Binder<[PhoneModel]>?

     init(completion : @escaping MainViewModelCompletion,
         failure : @escaping FailureErrorBlock) {

         super.init()

    }

    private  func transferModelData(model: [PhoneModel]) {
            self.model = Binder(val: model)
    }
}
