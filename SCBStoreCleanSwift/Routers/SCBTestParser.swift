//
//  Parser.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import SwiftyJSON

class SCBTestParser {

  class func parseArray(fromJsonArray json: [[String: Any]],
                        completionBlock: @escaping MultiPhonesCompletion) {

    DispatchQueue.main.async {
      var phoneModels = [PhoneModel]()

      for singleJSON in json {
        parse(fromJSON: singleJSON ,
              completionBlock: { (model) in

                phoneModels.append(model)
        })
      }

      completionBlock(phoneModels)

    }
  }

  class func parse(fromJSON json: [String: Any] ,
                   completionBlock: @escaping PhoneCompletion) {

    let swiftyJSON = JSON(json)

    let price  =  swiftyJSON["price"].float
    let thumb  =  swiftyJSON["thumbImageURL"].string
    let identity  =  swiftyJSON["id"].int
    let name = swiftyJSON["name"].string
    let brand = swiftyJSON["brand"].string
    let rate = swiftyJSON["rating"].float
    let description = swiftyJSON["description"].string

    let model =  PhoneModel.init(name: name ?? "", price: price ?? 0, thumbnailURL: thumb, identity: identity ?? 0, brand: brand ?? "", rating: rate ?? 0, description: description ?? "", isFavorite: false)
    
    completionBlock(model)

  }

  class func parseImageUrl(fromJsonArray json: [[String: Any]],
                           completionBlock : @escaping PhoneImageCompletion) {
    DispatchQueue.main.async {
      var phoneModels = [PhoneImageModel]()
      for singleJSON in json {
        let single = JSON(singleJSON)
        let identity = single["mobile_id"].int
        let imageURL = single["url"].string
        let imagemodel = PhoneImageModel(imageURL: imageURL ?? "",
                                         identity: identity ?? 0)
        phoneModels.append(imagemodel)
      }
      completionBlock(phoneModels)
    }
  }

}
