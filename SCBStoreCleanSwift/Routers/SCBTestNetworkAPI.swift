//
//  NetworkAPI.swift
//  SCBStore
//
//  Created by XXD on 25/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

class SCBTestNetworkAPI {

  class func fetchAllMobiles (completion : @escaping MultiPhonesCompletion ,
                              failureBlock : @escaping FailureErrorBlock) {

    let router = SCBTestPhoneRouter.allphones
    let task = SCBTestNetworkRequest.request(router,
                                             completionBlock: { (jsonresponse) in

                                              guard let json = jsonresponse as? [[String: Any]] else {
                                                return failureBlock(SCError(code: 0, desc: "Unexpect Json Signature", error: nil)) }

                                              SCBTestParser.parseArray(fromJsonArray: json,
                                                                       completionBlock: { (phonesmodels) in

                                                                        completion(phonesmodels)
                                              })

    }) { (err) in
      failureBlock(err)
    }
    guard let taskExist = task else { return }
    taskExist.resume()
  }

  class func fetchImagesForMobile (number: String ,
                                   completion : @escaping PhoneImageCompletion ,
                                   failureBlock : @escaping FailureErrorBlock) {

    let router = SCBTestPhoneRouter.phoneImages(location: number)
    
    let task = SCBTestNetworkRequest.request(router,
                                             completionBlock: { (jsonresponse) in
                                              guard let json = jsonresponse as? [[String: Any]] else {
                                                return failureBlock(SCError(code: 0, desc: "Unexpect Json Signature", error: nil)) }

                                              SCBTestParser.parseImageUrl(fromJsonArray: json,
                                                                          completionBlock: { (phoneurlmodels) in
                                                                            completion(phoneurlmodels)
                                              })

    }) { (err) in

      failureBlock(err)
    }
    guard let taskExist = task else { return }
    taskExist.resume()
  }
}
