//
//  NetworkRequest.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import Alamofire

class SCBTestNetworkRequest: NSObject, URLSessionDelegate, URLSessionTaskDelegate {

    static let shared = SCBTestNetworkRequest()

    private let manager: SessionManager = SessionManager.default

    var sesmanager: URLSession?

    private override init() {
        super.init()
        sesmanager = URLSession.init(configuration: .default, delegate: self, delegateQueue: nil)

    }

    class func request(_ url: URLRequestConvertible,
                       completionBlock: @escaping ObjectCompletionBlock<Any>,
                       failureBlock: @escaping FailureErrorBlock) -> URLSessionTask? {

        let ses =  RestHolder.shared.sesmanager
        guard let req = url.urlRequest else { return nil }
        let method = req.httpMethod
      
        if  RestHolder.shared.taskholder != nil {
            RestHolder.shared.taskholder?.cancel()
            RestHolder.shared.taskholder = nil
        }

        let task = ses.dataTask(with: req) { (data, res, err) in
            if let response = res, let jsondata = data {
                do {
                        //expect  Dictionary as top of JSON level
                    let jsondic =  try  JSONSerialization.jsonObject(with: jsondata, options: [])

                        //  print("(RestController) Show JSON ==> \(jsondic)")
                    let HTTPresponse = response as? HTTPURLResponse
                  let responseStatus  = HTTPresponse?.statusCode

                    if responseStatus == 200 {
                        //  print("(RestController) response OK")
                        completionBlock(jsondic)

                    } else if responseStatus == 202 && method == "POST" {
                        //  print("(RestController) response OK")
                        completionBlock(jsondic)

                    } else {
                        if let error = err {

                            let desc = error.localizedDescription
                          failureBlock(SCError(code: responseStatus ?? 520, desc: desc, error: error))
                        }
                    }
                } catch {    }
            }
        }
        RestHolder.shared.taskholder = task
        return task
    }
}

class RestHolder: NSObject {

    static let shared = RestHolder()

    public var sesmanager: URLSession = URLSession(configuration: .default)

    public var taskholder: URLSessionDataTask?

}
