//
//  NetworkRouter.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import Alamofire

enum SCBTestPhoneRouter: URLRequestConvertible {

    case allphones
    case phoneImages(location:String)

    var method: HTTPMethod {
        switch self {
        case .allphones, .phoneImages :
            return .get
        }
    }

    var url: String {
        switch self {

        case .allphones, .phoneImages:
            return SCBBaseURL
        }
    }

    var path: String {
        switch self {

        case .allphones:
            return SCBAllMobilesPath

        case .phoneImages(let location):
            return SCBAllMobilesPath + location + SCBSpecifyMobileImagesPath

        }
    }

    var finalUrl: URL? {
        switch self {

        case .allphones, .phoneImages:

            let url =  URL(string: self.url + self.path)
            let newURL = url
            guard let theURL = newURL else {
               return  nil
            }
            return theURL
        }
    }

    func asURLRequest() throws -> URLRequest {
        guard let finalURL = self.finalUrl else { return URLRequest(url: URL(string:"")!) }
        var urlRequest = URLRequest(url: finalURL)
        urlRequest.httpMethod = method.rawValue

        switch self {

        case .allphones, .phoneImages:

            return urlRequest
        }
    }
}
