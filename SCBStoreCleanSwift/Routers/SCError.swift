//
//  SCError.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

public class SCError: Error {

    var code: Int = 0
    var desc: String = ""
    var error: Error?

    init() { }

    init(code: Int, desc: String, error: Error?) {
        self.code = code
        self.desc = desc
        self.error = error
    }

}
