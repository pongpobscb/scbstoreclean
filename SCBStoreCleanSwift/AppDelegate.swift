//
//  AppDelegate.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 13/5/2562 BE.
//  Copyright © 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    return true
  }
}
