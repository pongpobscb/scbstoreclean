//
//  SCBaseViewModel.swift
//  SCBStore
//
//  Created by XXD on 27/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

class Binder <T> {

    var value: T {
        didSet {
            DispatchQueue.main.async {
                 guard let valChange = self.valueChange else { return }
                 valChange(self.value)
            }
        }
    }

    // bind callback , implement in VC
    var valueChange: ((T) -> Void)? = {(anything) in
        // do nothing
    }

    init (val: T ) {
        value = val
    }
}

protocol SCBaseViewModel: class {

    // bind to VC

    var model: Binder<[PhoneModel]>? { get }

}

protocol SCBBaseDetailViewModel: class {

    var model: Binder<[PhoneImageModel]>? { get }

}
