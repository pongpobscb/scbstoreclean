//
//  BaseTableViewController.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import UIKit

class BaseTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "PhoneDisplayTVCCell", bundle: nil),
                                forCellReuseIdentifier: "PhoneDisplayTVCCell")
        self.tableView.separatorStyle = .none
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {

         guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneDisplayTVCCell",
                            for: indexPath) as? PhoneDisplayTVCCell else {
         return  UITableViewCell(style: .default, reuseIdentifier: "cell")
         }
      
         cell.startSetup()
         cell.phoneTextView.text = ""
         return cell
    }

    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 112
    }

}
