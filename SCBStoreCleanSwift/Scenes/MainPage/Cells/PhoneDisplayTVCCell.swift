//
//  PhoneTableViewCell.swift
//  SCBStore
//
//  Created by XXD on 25/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import UIKit

class PhoneDisplayTVCCell: UITableViewCell {
  
  @IBOutlet weak var phoneImage: UIImageView!
  
  @IBOutlet weak var phoneTitle: UILabel!
  
  @IBOutlet weak var phoneTextView: UITextView!
  
  @IBOutlet weak var phonePrice: UILabel!
  
  @IBOutlet weak var phoneRating: UILabel!
  
  @IBOutlet weak var phoneFavoriteStar: UIButton!
  
  var isFavorite: Bool = false
  
  var favoriteClosure : (() -> Void)?
  var defavoriteClosure : (() -> Void)?
  
  @IBAction func phoneFavoriteClicked(_ sender: Any) {
    
    // deselect
    if isFavorite == true {
      //starSelected(on: false)
      buttonStateControl()
      isFavorite = false
      if let defav = defavoriteClosure {
        defav()
      }
      // select
    } else {
      //starSelected(on: true)
      buttonStateControl()
      isFavorite = true
      if let fav = favoriteClosure {
        fav()
      }
    }
  }
  
  func presentData(onemodel: PhoneModel, indexPath:  IndexPath) {
    
    phoneTitle.text = onemodel.name
    phoneFavoriteStar.setImage(UIImage(named: "starEmpty"), for: .normal)
    phoneFavoriteStar.setImage(UIImage(named: "starHighlight"), for: .selected)
    phoneFavoriteStar.addTarget(self, action: #selector(buttonStateControl), for: UIControl.Event.touchUpInside)
    phoneFavoriteStar.isSelected = false
    
    phoneRating.text =  "Rating: " + "\(String(describing: onemodel.rating))"
    phonePrice.text =   "Price: $" + "\(String(describing: onemodel.price))"
    phoneTextView.text = onemodel.description
    _ = phoneImage.imageQuery(address: onemodel.thumbnailURL)
    starSelected(on: onemodel.isFavorite)
    
    favoriteClosure = { () in
      NotificationCenter.default.post(name: .FavoriteSelected,
        object: nil, userInfo: ["index" :  indexPath.row])}
    
    defavoriteClosure = { () in
      NotificationCenter.default.post(name: .FavoriteDeselected,
        object: nil , userInfo: ["index" :  indexPath.row])}
  }
  
  
//  func starSelected (on: Bool) {
//    switch on {
//    case true: phoneFavoriteStar.setImage(UIImage(named: "starHighlight"), for: .normal)
//    case false: phoneFavoriteStar.setImage(UIImage(named: "starEmpty"), for: .normal)
//    }
//  }
  
  
  @objc func buttonStateControl(){
    if isFavorite == true {
       starSelected(on: false)
    }else {
       starSelected(on: true)
    }
  }
  
  func starSelected (on: Bool) {
    switch on {
    case true: phoneFavoriteStar.isSelected = true
    case false: phoneFavoriteStar.isSelected = false
    }
  }
  
  func startSetup() {
    phoneTextView.textContainer.maximumNumberOfLines = 2
    phoneTextView.textContainer.lineBreakMode = .byTruncatingTail
  }
  
  func hideStar() {
    phoneFavoriteStar.isHidden = true
    phoneFavoriteStar.isUserInteractionEnabled = false
  }
  
}
