//
//  FavoriteViewModel.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

class FavoriteViewModel: NSObject, SCBaseViewModel {

    var model: Binder<[PhoneModel]>?

    // TVC conformance

    var sectionNumber: Int {
        return 1
    }
    var rowNumber: Int {
        return model?.value.count ?? 0
    }

    // init
    init(originalmodel: [PhoneModel], sortby: Sortby) {
        super.init()
        let favorites = filterFavorite(phones: originalmodel)
        let newsorted = sorting(phones: favorites, by: sortby)
        self.transferModelData(model: newsorted)
    }

    func newsort (by: Sortby) {
        guard let modelexist = model else { return }
        let newsorted = sorting(phones: modelexist.value, by: by)
        self.transferModelData(model: newsorted)
    }

    func delete (index: Int) {
         model?.value.remove(at: index )
    }

    private  func transferModelData(model: [PhoneModel]) {
        self.model = Binder(val: model)
    }

    private func filterFavorite(phones: [PhoneModel]) -> [PhoneModel] {
        let favorites =   phones.filter { $0.isFavorite == true }
        return favorites
    }

    private func sorting(phones: [PhoneModel], by: Sortby) -> [PhoneModel] {

        switch by {
        case .priceHighToLow:
            return phones.sorted(by: { $0.price > $1.price })
        case .priceLowToHigh:
            return phones.sorted(by: { $0.price < $1.price })
        case .ratingHighToLow:
            return phones.sorted(by: { $0.rating > $1.rating })
        }
    }

}
