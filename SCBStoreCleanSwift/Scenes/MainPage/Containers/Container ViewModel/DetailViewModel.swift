//
//  DetailViewModel.swift
//  SCBStore
//
//  Created by XXD on 27/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

class DetailViewModel: NSObject {

    var imagemodel: Binder<[PhoneImageModel]>?

    var name: String? = ""
    var price: String? = ""
    var rating: String? = ""
    var desc: String? = ""

    init(originalModel: PhoneModel ,
            getViewmodelCompletion : @escaping DetailViewModelCompletion ,
            failure : @escaping FailureErrorBlock) {
        super.init()
        self.name = originalModel.name
        self.price =  "Price : $" + "\(String(describing: originalModel.price))"
        self.rating = "Rating :" + "\(String(describing: originalModel.rating))"
        self.desc = originalModel.description

        let iden = originalModel.identity

        SCBTestNetworkAPI.fetchImagesForMobile(number: "\(String(describing: iden))",
            completion: { (imageModels) in

            self.imagemodel = Binder(val: imageModels )
            getViewmodelCompletion(self)

        }) { (err) in
            failure(err)
        }

    }

}
