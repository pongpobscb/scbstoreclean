//
//  ListViewModel.swift
//  SCBStore
//
//  Created by XXD on 26/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation

class ListViewModel: NSObject, SCBaseViewModel {

    var model: Binder<[PhoneModel]>?

    // TVC conformance

    var sectionNumber: Int {
        return 1
    }
    var rowNumber: Int {
        return model?.value.count ?? 0
    }

    // init

    init(originalmodel: [PhoneModel], sortby: Sortby) {
        super.init()

        let newsorted = sorting(phones: originalmodel, by: sortby)
        self.transferModelData(model: newsorted)

        self.model?.valueChange = {(phonemodel) in
            // do nothing
        }
    }

    // favorite control

    // by Indexpath
    func addFavorite (favorite: Bool, toIndex: Int) {
         self.model?.value[toIndex].isFavorite = favorite
    }
    // by identity
    func addFavoriteByIdentity (favorite: Bool, toIdentity: Int) {

        if let index = self.model?.value.index(where: { $0.identity == toIdentity }) {
            self.model?.value[index].isFavorite = favorite
        }

    }
    // sort option

    func newsort (by: Sortby) {
        guard let modelexist = model else { return }
        let newsorted = sorting(phones: modelexist.value, by: by)
        self.transferModelData(model: newsorted)
    }

    private  func transferModelData(model: [PhoneModel]) {
        self.model = Binder(val: model)
    }

    private func sorting(phones: [PhoneModel], by: Sortby) -> [PhoneModel] {

        switch by {
            case .priceHighToLow:
                   return phones.sorted(by: { $0.price > $1.price })
            case .priceLowToHigh:
                   return phones.sorted(by: { $0.price < $1.price })
            case .ratingHighToLow:
                   return phones.sorted(by: { $0.rating > $1.rating })
        }
    }
}
