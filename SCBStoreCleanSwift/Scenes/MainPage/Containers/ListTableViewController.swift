//
//  ListTableViewController.swift
//  SCBStore
//
//  Created by XXD on 25/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import UIKit


protocol ListTableViewNavigationProtocol {
  func listNavigateToDetail (phone: PhoneModel)
}

class ListTableViewController: BaseTableViewController {
  
  var parentVCDelegate : ListTableViewNavigationProtocol?
  var viewmodel: ListViewModel?
  var currentSortMode: Sortby? = .priceHighToLow
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.tableView.delaysContentTouches = false
    NotificationCenter.default.addObserver(self, selector: #selector(favoriteStarSelect),
                                           name: .FavoriteSelected, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(favoriteStarDeselect),
                                           name: .FavoriteDeselected, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(favoriteDeleted),
                                           name: .FavoriteDeleted, object: nil)

  }

  @objc func favoriteStarSelect (notify: NSNotification) {
    
    guard let resultindex = notify.userInfo?["index"] else { return }
  
  if resultindex is Int {
    viewmodel?.addFavorite(favorite: true, toIndex: resultindex as! Int)
    let main = self.parentVCDelegate as? SCMainViewController
    main?.transferModelListToFavorite()
    self.tableView.reloadData()
    }
  }

  @objc func favoriteStarDeselect (notify: NSNotification) {
    guard let resultindex = notify.userInfo?["index"] else { return }
    if resultindex is Int {
      viewmodel?.addFavorite(favorite: false, toIndex: resultindex as! Int )
      let main = self.parentVCDelegate as? SCMainViewController
      main?.transferModelListToFavorite()
      self.tableView.reloadData()
    }
  }

  @objc func favoriteDeleted (notify: NSNotification) {

    guard let resultidentity = notify.userInfo?["identity"] else { return }
    if resultidentity is Int {
      viewmodel?.addFavoriteByIdentity (favorite: false, toIdentity: resultidentity as! Int)
      let main = self.parentVCDelegate as? SCMainViewController
      main?.transferModelListToFavorite()
      self.tableView.reloadData()

    }
  }
}

extension ListTableViewController {
  override func numberOfSections(in tableView: UITableView) -> Int {
    return viewmodel?.sectionNumber ?? 0
  }

  override func tableView(_ tableView: UITableView,
                          numberOfRowsInSection section: Int) -> Int {
    return viewmodel?.rowNumber ?? 0
  }

  override func tableView(_ tableView: UITableView,
                          didSelectRowAt indexPath: IndexPath) {

    guard let arraymodels =   self.viewmodel?.model?.value else { return }
    let onemodel = arraymodels[indexPath.row]
    
    let main = self.parentVCDelegate as? SCMainViewController
    main?.FavoriteNavigateToDetail(phone: onemodel)
    
  }

  override func tableView(_ tableView: UITableView,
                          cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneDisplayTVCCell",
                                                   for: indexPath) as? PhoneDisplayTVCCell else {
                          return UITableViewCell(style: .default, reuseIdentifier: "cell")
    }
    cell.startSetup()

    if let displaymodels = viewmodel?.model?.value {
      let onemodel = displaymodels[indexPath.row]
      cell.presentData(onemodel: onemodel, indexPath: indexPath)

    }

    return cell
  }

}
