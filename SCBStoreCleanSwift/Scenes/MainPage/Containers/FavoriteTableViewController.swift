//
//  FavoriteTableViewController.swift
//  SCBStore
//
//  Created by XXD on 25/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import UIKit

protocol FavoriteTableViewNavigationProtocol {
    func FavoriteNavigateToDetail (phone: PhoneModel)
}

class FavoriteTableViewController: BaseTableViewController {
  
    var parentVCDelegate : FavoriteTableViewNavigationProtocol?

    //var motherVC: SCMainViewController?
    var viewmodel: FavoriteViewModel?
    var currentSortMode: Sortby? = .priceHighToLow
  
    override func viewDidLoad() {
        super.viewDidLoad()
     }

 }

extension FavoriteTableViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewmodel?.sectionNumber ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewmodel?.rowNumber ?? 0
    }

    override func tableView(_ tableView: UITableView,
                         trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let action = UIContextualAction(style: .normal, title: "Delete",
                                        handler: { (_, _, completionHandler) in

               let currentobj =  self.viewmodel?.model?.value[indexPath.row]
               if let identifier = currentobj?.identity {
                      NotificationCenter.default.post(name: .FavoriteDeleted,
                                                object: nil, userInfo: ["identity": identifier])
               }
               completionHandler(true)
        })

        action.backgroundColor = UIColor.red
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {

       guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneDisplayTVCCell",
                                                      for: indexPath) as? PhoneDisplayTVCCell  else {
        return UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        cell.hideStar()
        cell.startSetup()

        if let displaymodels = viewmodel?.model?.value {
            let onemodel = displaymodels[indexPath.row]
            cell.presentData(onemodel: onemodel, indexPath: indexPath)
        }
        return cell
    }
  
    override func tableView(_ tableView: UITableView,
                          didSelectRowAt indexPath: IndexPath) {
    
      guard let arraymodels =   self.viewmodel?.model?.value else { return }
      let onemodel = arraymodels[indexPath.row]
    
      let main = self.parentVCDelegate as? SCMainViewController
      main?.FavoriteNavigateToDetail(phone: onemodel)
    
  }

 }
