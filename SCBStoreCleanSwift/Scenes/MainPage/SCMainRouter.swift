//
//  SCMainRouter.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 13/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol SCMainRouterInput {
  func navigateToSomewhere()
}

class SCMainRouter: SCMainRouterInput {
  weak var viewController: SCMainViewController!
  var dataStore: SCMainInteractor?

  // MARK: - Routing

  func routeToDetailVC (segue: UIStoryboardSegue? ,
                        from: UIViewController, phone: PhoneModel ) {

    let sb = UIStoryboard(name: "Main", bundle: nil)
    let nextvc = sb.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController

    nextvc.interactor.detailModel = phone
    from.navigationController?.pushViewController(nextvc, animated: true)

  }

  // MARK: - Navigation

  func navigateToSomewhere() {
    // NOTE: Teach the router how to navigate to another scene. Some examples follow:

    // 1. Trigger a storyboard segue
    // viewController.performSegueWithIdentifier("ShowSomewhereScene", sender: nil)

    // 2. Present another view controller programmatically
    // viewController.presentViewController(someWhereViewController, animated: true, completion: nil)

    // 3. Ask the navigation controller to push another view controller onto the stack
    // viewController.navigationController?.pushViewController(someWhereViewController, animated: true)

    // 4. Present a view controller from a different storyboard
    // let storyboard = UIStoryboard(name: "OtherThanMain", bundle: nil)
    // let someWhereViewController = storyboard.instantiateInitialViewController() as! SomeWhereViewController
    // viewController.navigationController?.pushViewController(someWhereViewController, animated: true)
  }

  // MARK: - Communication

}
