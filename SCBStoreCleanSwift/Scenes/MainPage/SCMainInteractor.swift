//
//  SCMainInteractor.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 13/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol SCMainInteractorInterface {
  func fetchData(request: SCMain.DisplayData.Request)
  var model: [PhoneModel]? { get }
}

protocol SMDataStoreInterface {
  var model: [PhoneModel]? { get set }
}

class SCMainInteractor: (SCMainInteractorInterface & SMDataStoreInterface) {

  var presenter: SCMainPresenterInterface!
  var worker: SCMainWorker?
  var model: [PhoneModel]?

  // MARK: - Business logic

  func fetchData(request: SCMain.DisplayData.Request) {

    worker?.fetchAllPhones({(phones) in
      if phones.count != 0 {
        self.model = phones
      }
      let response =  SCMain.DisplayData.Response(result: .success(phones))
      self.presenter.presentPhones(response: response)

    }, failure: { (err) in
      let response = SCMain.DisplayData.Response(result: .failure(err))
      self.presenter.presentPhones(response: response)
      })
    }
  }

