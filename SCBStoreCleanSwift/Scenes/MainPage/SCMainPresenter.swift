//
//  SCMainPresenter.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 13/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol SCMainPresenterInterface {
  // func presentSomething(response: SCMain.Something.Response)
  func presentPhones (response: SCMain.DisplayData.Response)
}

class SCMainPresenter: SCMainPresenterInterface {
  weak var viewController: SCMainViewControllerInterface!

  // MARK: - Presentation logic

  func presentPhones(response: SCMain.DisplayData.Response) {
  
    switch response.result {
    case .success:
          viewController.displayPhones(result: response)
          
    case .failure:
          viewController.displayPhones(result: response)
    }
  }

}
