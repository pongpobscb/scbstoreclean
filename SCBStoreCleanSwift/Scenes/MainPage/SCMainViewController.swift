//
//  SCMainViewController.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 13/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol SCMainViewControllerInterface: class {

  // func displayPhones(viewModel: SCMain.DisplayData.ViewModel)
  func displayPhones(result: SCMain.DisplayData.Response)

}

class SCMainViewController: BaseViewController, SCMainViewControllerInterface {

  @IBOutlet weak var FirstContainer: UIView!

  @IBOutlet weak var SecondContainer: UIView!

  @IBOutlet weak var ListAllButton: UIButton!

  @IBOutlet weak var FavoriteButton: UIButton!

  @IBAction func AllAction(_ sender: Any) {
    // display All ContainerView
    // hide FavView
    displayDecision(display: .first)
  }

  @IBAction func FavoriteAction(_ sender: Any) {
    // display Fav ContainerView
    // hide allView
    displayDecision(display: .second)
  }

  var interactor: SCMainInteractorInterface!
  var router: SCMainRouter!

  var showAllPhonesTVC: ListTableViewController?
  var showFavoriteTVC: FavoriteTableViewController?
  var currentDisplay: displayMainView? = .first
  var currentSortMode: Sortby? = .priceHighToLow

  // MARK: - Object lifecycle

  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }
  override func viewDidLayoutSubviews() {
    
    showAllPhonesTVC?.parentVCDelegate = self
    showFavoriteTVC?.parentVCDelegate = self
  }
  // MARK: - Configuration

  private func configure(viewController: SCMainViewController) {
    let router = SCMainRouter()
    router.viewController = viewController

    let presenter = SCMainPresenter()
    presenter.viewController = viewController

    let interactor = SCMainInteractor()
    interactor.presenter = presenter
    interactor.worker = SCMainWorker(store: SCMainStore())

    viewController.interactor = interactor
    viewController.router = router
    // interactor play role as data store
    viewController.router.dataStore =  interactor
  }

  // MARK: - View lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    addSortBarButton()
    displayDecision(display: .first)
    fetchDataOnLoad()
  }

  // MARK: - Event handling

  func fetchDataOnLoad() {
    // NOTE: Ask the Interactor to do some work
    let request = SCMain.DisplayData.Request()
    interactor.fetchData(request: request)
  }

  // MARK: - Display logic
  
  
  func displayPhones(result: SCMain.DisplayData.Response){
    
    switch  result.result {
        case .success (let data):
              self.transferModel(originalviewmodel: data)
              self.showAllPhonesTVC?.tableView.reloadData()
              self.showFavoriteTVC?.tableView.reloadData()
        case .failure :
              self.displayErrorAlert(msg: "Error Code unknown" )
    }
  }
  
  // MARK: - Router

  
}

extension SCMainViewController : FavoriteTableViewNavigationProtocol ,
                                 ListTableViewNavigationProtocol {
  
  func FavoriteNavigateToDetail(phone: PhoneModel) {
    self.router.routeToDetailVC(segue: nil, from: self, phone: phone)
  }
  
  func listNavigateToDetail(phone: PhoneModel) {
    self.router.routeToDetailVC(segue: nil, from: self, phone: phone)
  }
  
}

extension SCMainViewController {

  private func displayDecision (display: displayMainView) {

    switch display {

    case .first :
      FirstContainer.alpha = 1.0
      FirstContainer.isUserInteractionEnabled = true
      SecondContainer.alpha = 0.0
      SecondContainer.isUserInteractionEnabled = false
      ListAllButton.setTitleColor(.black, for: .normal)
      FavoriteButton.setTitleColor(.gray, for: .normal)

    case .second :
      FirstContainer.alpha = 0.0
      FirstContainer.isUserInteractionEnabled = false
      SecondContainer.alpha = 1.0
      SecondContainer.isUserInteractionEnabled = true
      ListAllButton.setTitleColor(.gray, for: .normal)
      FavoriteButton.setTitleColor(.black, for: .normal)
    }
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    if let tvc = segue.destination as? ListTableViewController,
      segue.identifier == "toFirstTVC" {
      self.showAllPhonesTVC = tvc
      
 
      if let modelvalue = self.interactor.model {
        self.showAllPhonesTVC?.viewmodel =
          ListViewModel(originalmodel: modelvalue, sortby: .priceHighToLow)
      }
        
    } else if let tvc2 = segue.destination as? FavoriteTableViewController,
      segue.identifier == "toSecondTVC" {
      self.showFavoriteTVC = tvc2
       // copy from FirstTVC
      if let modelvalue = self.showAllPhonesTVC?.viewmodel?.model?.value {
        self.showFavoriteTVC?.viewmodel =
          FavoriteViewModel(originalmodel: modelvalue, sortby: .priceHighToLow)

      }
    }
  }

  private func transferModel(originalviewmodel: [PhoneModel]?) {
    if let modelvalue = originalviewmodel {
      self.showAllPhonesTVC?.viewmodel =
        ListViewModel(originalmodel: modelvalue,
                      sortby: currentSortMode ?? .priceHighToLow)

      if let listmodelvalue = self.showAllPhonesTVC?.viewmodel?.model?.value {
        self.showFavoriteTVC?.viewmodel = FavoriteViewModel(originalmodel: listmodelvalue, sortby: currentSortMode ?? .priceHighToLow)
      }
    }
  }

  func transferModelListToFavorite () {
    if let listmodelvalue = self.showAllPhonesTVC?.viewmodel?.model?.value {
      self.showFavoriteTVC?.viewmodel = FavoriteViewModel(originalmodel: listmodelvalue, sortby: currentSortMode ?? .priceHighToLow)
      self.showFavoriteTVC?.tableView.reloadData()
    }
  }

  private func displaySortOptions () {

    let options = UIAlertController(title: "Sort",
                                    message: nil, preferredStyle: .alert)

    options.addAction(UIAlertAction(title: "Price low to high", style: .default, handler: { _ in
      self.currentSortMode = .priceLowToHigh
      self.showAllPhonesTVC?.viewmodel?.newsort(by: .priceLowToHigh)
      self.showFavoriteTVC?.viewmodel?.newsort(by: .priceLowToHigh)

      self.showAllPhonesTVC?.tableView.reloadData()
      self.showFavoriteTVC?.tableView.reloadData()
    }))
    options.addAction(UIAlertAction(title: "Price high to low", style: .default, handler: { _ in
      self.currentSortMode = .priceHighToLow
      self.showAllPhonesTVC?.viewmodel?.newsort(by: .priceHighToLow)
      self.showFavoriteTVC?.viewmodel?.newsort(by: .priceHighToLow)

      self.showAllPhonesTVC?.tableView.reloadData()
      self.showFavoriteTVC?.tableView.reloadData()
    }))
    options.addAction(UIAlertAction(title: "Rating", style: .default, handler: { _ in
      self.currentSortMode = .ratingHighToLow
      self.showAllPhonesTVC?.viewmodel?.newsort(by: .ratingHighToLow)
      self.showFavoriteTVC?.viewmodel?.newsort(by: .ratingHighToLow)

      self.showAllPhonesTVC?.tableView.reloadData()
      self.showFavoriteTVC?.tableView.reloadData()
    }))
    options.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { _ in
      //Cancel
    }))
    self.present(options, animated: true, completion: nil)
  }

  private func addSortBarButton () {
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sort",
                                                             style: .plain,
                                                             target: self,
                                                             action: #selector(sortAction))
  }

  @objc private  func sortAction() {
    displaySortOptions()
  }
}
