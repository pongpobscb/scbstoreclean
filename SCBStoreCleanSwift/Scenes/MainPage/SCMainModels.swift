//
//  SCMainModels.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 13/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

struct SCMain {
    struct DisplayData {
        /// Data struct sent to Interactor
        struct Request {}
        /// Data struct sent to Presenter
        struct Response {
          var result: Result<[PhoneModel]>
        }
        /// Data struct sent to ViewController
        struct ViewModel {

        }
    }

}
