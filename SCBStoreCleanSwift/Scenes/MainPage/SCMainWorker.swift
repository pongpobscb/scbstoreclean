//
//  SCMainWorker.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 13/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol SCMainStoreProtocol {
   func getPhones(_ completion: @escaping MultiPhonesCompletion ,
                  failure : @escaping FailureErrorBlock)
}

class SCMainWorker {

  var store: SCMainStoreProtocol

  init(store: SCMainStoreProtocol) {
    self.store = store
  }

  // MARK: - Business Logic

  func fetchAllPhones(_ completion: @escaping MultiPhonesCompletion,
                        failure : @escaping FailureErrorBlock) {

    store.getPhones({ (phones) in
        completion(phones)
    }) { (error) in
        failure(error)
    }
  }
}
