//
//  SCMainStore.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 13/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import Foundation

/*
 
 The SCMainStore class implements the SCMainStoreProtocol.
 
 The source for the data could be a database, cache, or a web service.
 
 You may remove these comments from the file.
 
 */

class SCMainStore: SCMainStoreProtocol {
  func getPhones(_ completion: @escaping MultiPhonesCompletion,
                 failure: @escaping FailureErrorBlock) {

    // DispatchQueue.main.asyncAfter(deadline: .now() + 30) {

    SCBTestNetworkAPI.fetchAllMobiles(completion: { (phones) in
      //print("ALL  = \(phones)")
      completion(phones)
    }, failureBlock: { (err) in
      // failure handle
      failure(err)
    })
    //  }
  }

}
