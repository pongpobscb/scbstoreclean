//
//  PhoneDetailCVC.swift
//  SCBStore
//
//  Created by XXD on 25/2/2562 BE.
//  Copyright © 2562 Wazabidev. All rights reserved.
//

import Foundation
import UIKit

class PhoneDetailCVC: UICollectionViewController {

    var imagemodel: [PhoneImageModel]?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self

        self.collectionView?.showsVerticalScrollIndicator = false
        self.collectionView?.showsHorizontalScrollIndicator = false
        self.collectionView?.isPagingEnabled = true
        self.collectionView?.alwaysBounceVertical = false
        self.collectionView?.alwaysBounceHorizontal = false
        self.collectionView?.contentInsetAdjustmentBehavior = .never

        let lay = self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        lay?.scrollDirection = .horizontal

        self.collectionView?.register(UINib(nibName: "PhoneDetailCVCCell", bundle: nil),
                                      forCellWithReuseIdentifier: "PhoneDetailCVCCell")
    }
}

extension PhoneDetailCVC: UICollectionViewDelegateFlowLayout {

    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhoneDetailCVCCell",
                                                            for: indexPath) as! PhoneDetailCVCCell
      
        if let imagesentity = imagemodel {
              let currentImage =  imagesentity[indexPath.row]
              let imagepath = currentImage.imageURL
               _ =  cell.PhoneImage.imageQuery(address: imagepath)
        }

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {

        return  imagemodel?.count ?? 0
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize =   UIScreen.main.bounds

        return CGSize(width: (screenSize.width * 0.5), height: (screenSize.height * 0.35))
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
