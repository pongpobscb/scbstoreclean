//
//  DetailPresenter.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 15/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol DetailPresenterInterface {
  func presentDetail(response: Detail.DetailDisplay.Response)
}

class DetailPresenter: DetailPresenterInterface {
  weak var viewController: DetailViewControllerInterface!

  // MARK: - Presentation logic
  
    func presentDetail(response: Detail.DetailDisplay.Response) {

        var detailToDisplay =  Detail.DetailDisplay.ViewModel()
        detailToDisplay.name =  response.rawDetailVM?.name
        detailToDisplay.price = response.rawDetailVM?.price
        detailToDisplay.rating = response.rawDetailVM?.rating
        detailToDisplay.desc = response.rawDetailVM?.desc
        detailToDisplay.imagemodel = response.rawDetailVM?.imagemodel?.value

        viewController.displayDetailViewModel(model: detailToDisplay)

    }
}
