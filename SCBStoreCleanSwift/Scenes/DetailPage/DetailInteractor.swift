//
//  DetailInteractor.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 15/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol DetailInteractorInterface {

  var model: Entity? { get }
  var detailModel: PhoneModel? { get set }

  func fetchData(originalmodel: PhoneModel ,
                 getViewmodelCompletion : @escaping DetailViewModelCompletion ,
                 failure : @escaping FailureErrorBlock)

  func fetchDetail(request: Detail.DetailDisplay.Request)
}

class DetailInteractor: DetailInteractorInterface {

  var presenter: DetailPresenterInterface!
  var worker: DetailWorker?
  var model: Entity?
  //
  var detailModel: PhoneModel?
  var detailDisplayViewModel: Detail.DetailDisplay.ViewModel?

  // MARK: - Business logic

  func fetchDetail(request: Detail.DetailDisplay.Request) {

    guard let detailModelExist = detailModel else { return }
      fetchData(originalmodel: detailModelExist,
                getViewmodelCompletion: { (_) in
      }) { (_) in

      }
  }
  
  func fetchData(originalmodel: PhoneModel ,
                 getViewmodelCompletion : @escaping DetailViewModelCompletion ,
                 failure : @escaping FailureErrorBlock ) {

    worker?.fetchImageAndDetail(originalmodel: originalmodel,
                                getViewmodelCompletion: { (detailviewmodel) in

                                  var response =  Detail.DetailDisplay.Response()
                                  response.rawDetailVM = detailviewmodel

                                  self.presenter.presentDetail(response: response)
                                  getViewmodelCompletion(detailviewmodel)

    }, failure: { (err) in

      failure(err)
    })

  }

}
