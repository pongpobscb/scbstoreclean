//
//  DetailModels.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 15/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

struct Detail {
  /// This structure represents a use case
  struct Something {
    /// Data struct sent to Interactor
    struct Request {}
    /// Data struct sent to Presenter
    struct Response {}
    /// Data struct sent to ViewController
    struct ViewModel {

    }
  }
    struct DetailDisplay {
        /// Data struct sent to Interactor
        struct Request {}
        /// Data struct sent to Presenter
        struct Response {
            var rawDetailVM: DetailViewModel?
        }
        /// Data struct sent to ViewController
        struct ViewModel {

            var imagemodel: [PhoneImageModel]?

            var name: String? = ""
            var price: String? = ""
            var rating: String? = ""
            var desc: String? = ""

        }
    }
}
