//
//  DetailViewController.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 15/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol DetailViewControllerInterface: class {
  func displaySomething(viewModel: Detail.Something.ViewModel)
  func displayDetailViewModel (model: Detail.DetailDisplay.ViewModel)
}

class DetailViewController: UIViewController, DetailViewControllerInterface {

  @IBOutlet weak var Rating: UILabel!
  @IBOutlet weak var Price: UILabel!
  @IBOutlet weak var Content: UITextView!
  @IBOutlet weak var clearBG: UIView!
  @IBOutlet weak var LoadingIndicator: UIActivityIndicatorView!

  var interactor: DetailInteractorInterface!
  var imageCollectionView: PhoneDetailCVC?

  // MARK: - Object lifecycle

  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }

  // MARK: - Configuration
  
  private func configure(viewController: DetailViewController) {
    
    let presenter = DetailPresenter()
    presenter.viewController = viewController

    let interactor = DetailInteractor()
    interactor.presenter = presenter
    interactor.worker = DetailWorker(store: DetailStore())

    viewController.interactor = interactor
    
  }

  // MARK: - View lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    doSomethingOnLoad()
    interfaceSetup()
    fetchDetail()
  }

  func fetchDetail () {
    let request = Detail.DetailDisplay.Request()
    interactor.fetchDetail(request: request)

  }

  func interfaceSetup() {
    self.addCustomBackButton()
    self.clearBG.alpha  = 0.3
    self.displayIndicator(active: true)
    self.LoadingIndicator.hidesWhenStopped = true
  }

  func displayIndicator (active: Bool) {

    switch active {
    case true :
      self.LoadingIndicator.startAnimating()

    case false :
      self.LoadingIndicator.stopAnimating()
    }
  }

  func displayDetailViewModel (model:
    Detail.DetailDisplay.ViewModel) {

    self.navigationItem.title = model.name
    self.Price.text =  model.price
    self.Rating.text =  model.rating
    self.Content.text = model.desc
    self.Content.isScrollEnabled = true 
    let imagemodels =  model.imagemodel
    self.imageCollectionView?.imagemodel = imagemodels
    self.imageCollectionView?.collectionView.reloadData()
    self.LoadingIndicator.stopAnimating()
  }

  // MARK: - Event handling

  func doSomethingOnLoad() {
    // NOTE: Ask the Interactor to do some work
  }

  // MARK: - Display logic

  func displaySomething(viewModel: Detail.Something.ViewModel) {
    // NOTE: Display the result from the Presenter

    // nameTextField.text = viewModel.name
  }

  // MARK: - Router

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 
    if let cvc = segue.destination as? PhoneDetailCVC,
      segue.identifier == "toPhoneDetailCVC"{
      self.imageCollectionView = cvc

    }
  }
}
