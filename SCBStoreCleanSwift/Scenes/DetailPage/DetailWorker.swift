//
//  DetailWorker.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 15/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import UIKit

protocol DetailStoreProtocol {
  func getData(_ completion: @escaping (Result<Entity>) -> Void)
  func getImageAndData (originalmodel: PhoneModel ,
                          getViewmodelCompletion : @escaping DetailViewModelCompletion ,
                          failure : @escaping FailureErrorBlock )
}

class DetailWorker {

  var store: DetailStoreProtocol

  init(store: DetailStoreProtocol) {
    self.store = store
  }

  // MARK: - Business Logic

    func fetchImageAndDetail (originalmodel: PhoneModel ,
                              getViewmodelCompletion : @escaping DetailViewModelCompletion ,
                              failure : @escaping FailureErrorBlock ) {

        store.getImageAndData(originalmodel: originalmodel, getViewmodelCompletion: { (detailviewmodel) in
             getViewmodelCompletion(detailviewmodel)
        }) { (err) in
             failure(err)
        }

    }

  func doSomeWork(_ completion: @escaping (Result<Entity>) -> Void) {
    // NOTE: Do the work
    store.getData {
      // The worker may perform some small business logic before returning the result to the Interactor
      completion($0)
    }
  }
}
