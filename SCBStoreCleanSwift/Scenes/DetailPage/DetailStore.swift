//
//  DetailStore.swift
//  SCBStoreCleanSwift
//
//  Created by PONGPOB NGAMKANOKWAN on 15/5/2562 BE.
//  Copyright (c) 2562 PONGPOB NGAMKANOKWAN. All rights reserved.
//

import Foundation

/*

 The DetailStore class implements the DetailStoreProtocol.

 The source for the data could be a database, cache, or a web service.

 You may remove these comments from the file.

 */

class DetailStore: DetailStoreProtocol {

    func getImageAndData (originalmodel: PhoneModel ,
                          getViewmodelCompletion : @escaping DetailViewModelCompletion ,
                          failure : @escaping FailureErrorBlock ) {

    _ =  DetailViewModel(originalModel: originalmodel, getViewmodelCompletion: { (detailviewmodel) in
              getViewmodelCompletion(detailviewmodel)
        }) { (err) in
              failure(err)
        }
    }

  func getData(_ completion: @escaping (Result<Entity>) -> Void) {
    // Simulates an asynchronous background thread that calls back on the main thread after 2 seconds
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
      completion(Result.success(Entity()))
    }
  }
}
